package we.test;

import org.springframework.stereotype.Component;
import we.entities.item.Item;
import we.entities.storage.Storage;

@Component
public class TestData {

    public final Storage STORAGE_1 = new Storage() {{
        setName("Test storage 1");
    }};

    public final Storage STORAGE_2 = new Storage() {{
        setName("Test storage 2");
    }};

    public final Item ITEM_1 = new Item() {{
        setName("Test item 1");
    }};

    public final Item ITEM_2 = new Item() {{
        setName("Test item 2");
    }};
}
