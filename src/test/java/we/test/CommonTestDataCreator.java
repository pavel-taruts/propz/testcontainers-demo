package we.test;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import we.orm.AbstractRepo;
import we.orm.singleStringId.AbstractSingleStringIdRepo;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CommonTestDataCreator implements InitializingBean {

    @Autowired
    private TestData testData;

    @Autowired
    private List<AbstractRepo<?>> repos;

    @Override
    public void afterPropertiesSet() throws Exception {

        Map<Class<?>, AbstractRepo<?>> repoMap = repos
                .stream()
                .collect(Collectors.toMap(
                        AbstractRepo::getClazz,
                        x -> x
                ));

        Field[] fields = TestData.class.getDeclaredFields();
        for (Field field : fields) {

            Object entity = field.get(testData);

            // Get all classes of the entity
            List<Class<?>> classes = new ArrayList<>();
            Class<?> fieldClass = entity.getClass();
            do {
                classes.add(fieldClass);
                fieldClass = fieldClass.getSuperclass();
            } while (fieldClass != Object.class);

            // Find repo
            AbstractRepo<?> singleStringIdRepo = null;
            for (Class<?> clazz : classes) {
                singleStringIdRepo = repoMap.get(clazz);
                if (singleStringIdRepo != null) {
                    break;
                }
            }

            // Validate repo found
            if (singleStringIdRepo == null) {
                throw new NullPointerException("Didn't find repo for test data field %s. Classes: %s".formatted(
                        field.getName(),
                        classes
                                .stream()
                                .map(Class::getSimpleName)
                                .collect(Collectors.joining(", "))
                ));
            }

            // Passing the entity to the create method of the repo
            Method createMethod = AbstractSingleStringIdRepo.class.getMethod("create", Object.class);
            entity = createMethod.invoke(singleStringIdRepo, entity);

            // Setting the returned entity back
            field.setAccessible(true);
            field.set(testData, entity);
        }
    }
}
