package we.test;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfiguration;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import org.junit.ClassRule;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.Profiles;
import org.springframework.core.env.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import we.test.conf.TestConfiguration;

import java.util.Map;

@ContextConfiguration(
        classes = {
                TestConfiguration.class,
        },
        initializers = {
                BaseTest.Initializer.class,
        }
)
@WebAppConfiguration
@TestPropertySource("classpath:application-test.properties")
public abstract class BaseTest {

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    protected TestData testData;

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

            ConfigurableEnvironment environment = configurableApplicationContext.getEnvironment();

            boolean embeddedDb = environment.acceptsProfiles(Profiles.of("test-embedded-db"));
            if (embeddedDb) {
                DB db = launchEmbeddedDb();
                PropertySource<?> testHikariProperties = getEmbeddedDbPropertySource(db);
                environment.getPropertySources().addFirst(testHikariProperties);
            }
        }

        private PropertySource<?> getEmbeddedDbPropertySource(DB db) {
            Map<String, Object> map = Map.of(
                    "hikari.driverClassName", "org.mariadb.jdbc.Driver",
                    "hikari.jdbcUrl", db.getConfiguration().getURL("test"),
                    "hikari.username", "test",
                    "hikari.password", "test"
            );
            return new MapPropertySource("testHikariProperties", map);
        }

        private DB launchEmbeddedDb() {

            DBConfiguration dbConfiguration = DBConfigurationBuilder
                    .newBuilder()
                    .setPort(0)
                    .build();

            try {
                DB db = DB.newEmbeddedDB(dbConfiguration);
                db.start();
                db.createDB("test", "test", "test");
                return db;
            } catch (ManagedProcessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
