package we.test.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("test-container-db")
@PropertySource("classpath:application-test-container-db.properties")
public class TestContainerDbConfiguration {
}
