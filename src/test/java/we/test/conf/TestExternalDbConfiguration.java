package we.test.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("test-external-db")
@PropertySource("classpath:application-test-external-db.properties")
public class TestExternalDbConfiguration {
}
