package we.entities.storage.item.balance;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import we.test.BaseTest;

import java.math.BigDecimal;

public class StorageItemBalanceRepoTest extends BaseTest {

    @Autowired
    private StorageItemBalanceRepo storageItemBalanceRepo;

    @Test
    @Transactional
    public void test() {
        StorageItemBalance storageItemBalance = new StorageItemBalance();
        storageItemBalance.setStorageId(testData.STORAGE_1.getId());
        storageItemBalance.setItemId(testData.ITEM_1.getId());
        storageItemBalance.setAmount(BigDecimal.valueOf(100));

        storageItemBalanceRepo.create(storageItemBalance);

        StorageItemBalance extracted = storageItemBalanceRepo.get(storageItemBalance);

        BigDecimal expectedAmount = BigDecimal.valueOf(100);
        BigDecimal actualAmount = extracted.getAmount();
        //noinspection SimplifiableAssertion
        Assert.assertTrue(expectedAmount.compareTo(actualAmount) == 0);
    }
}
