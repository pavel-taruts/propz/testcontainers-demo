package we.entities.storage.item.movement;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import we.entities.storage.item.balance.StorageItemBalanceRepo;
import we.entities.storage.item.transaction.StorageItemTransaction;
import we.entities.storage.item.transaction.StorageItemTransactionRepo;
import we.test.BaseTest;

import java.math.BigDecimal;
import java.util.List;

public class StorageMovementServiceTest extends BaseTest {

    @Autowired
    private StorageMovementService storageMovementService;

    @Autowired
    private StorageItemBalanceRepo storageItemBalanceRepo;

    @Autowired
    private StorageItemTransactionRepo storageItemTransactionRepo;

    @Test
    @Transactional
    public void testCreate() {

        StorageMovement storageMovement = new StorageMovement();

        storageMovement.setStorageToId(testData.STORAGE_1.getId());
        storageMovement.setItemId(testData.ITEM_1.getId());
        storageMovement.setAmount(BigDecimal.valueOf(100));

        storageMovementService.create(storageMovement);

        BigDecimal actualAmount = storageItemBalanceRepo.getAmount(
                testData.STORAGE_1.getId(),
                testData.ITEM_1.getId()
        );

        BigDecimal expectedAmount = BigDecimal.valueOf(100);

        //noinspection SimplifiableAssertion
        Assert.assertTrue(expectedAmount.compareTo(actualAmount) == 0);

        List<StorageItemTransaction> transactions = storageItemTransactionRepo.list();

        Assert.assertEquals(1, transactions.size());

        StorageItemTransaction transaction = transactions.get(0);

        //noinspection SimplifiableAssertion
        Assert.assertTrue(expectedAmount.compareTo(transaction.getAmount()) == 0);
    }
}
