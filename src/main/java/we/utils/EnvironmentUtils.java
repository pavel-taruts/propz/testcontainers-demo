package we.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

@Component
public class EnvironmentUtils {

    @Autowired
    private ConfigurableEnvironment configurableEnvironment;

    public Map<String, String> getPropertiesAndRemovePrefix(String prefix) {

        int prefixLength = prefix.length();

        Set<String> prefixedPropertyNames = getPropertyNamesStartingWith(prefix);

        Map<String, String> map = new HashMap<>();

        prefixedPropertyNames.forEach(prefixedPropertyName -> {
            String propertyName = prefixedPropertyName.substring(prefixLength);
            String propertyValue = configurableEnvironment.getProperty(prefixedPropertyName);
            map.put(propertyName, propertyValue);
        });

        return map;
    }

    public Set<String> getPropertyNamesStartingWith(String prefix) {
        return getPropertyNames(propertyName -> propertyName.startsWith(prefix));
    }

    public Set<String> getPropertyNames(Predicate<String> propertyNameMatches) {
        Set<String> set = new HashSet<>();
        for (PropertySource<?> propertySource : configurableEnvironment.getPropertySources()) {
            if (propertySource instanceof EnumerablePropertySource mapPropertySource) {
                String[] propertyNames = mapPropertySource.getPropertyNames();
                Arrays
                        .stream(propertyNames)
                        .filter(propertyNameMatches)
                        .forEach(set::add);
            }
        }
        return set;
    }
}
