package we.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DatabaseUriUtils {

    private static final Pattern JDBC_URL_DB_NAME_PATTERN = Pattern.compile("/(?<dbName>\\w+)(?>$|\\?)");

    public static String getDatabaseName(String jdbcUrlStr) {

        Matcher matcher = JDBC_URL_DB_NAME_PATTERN.matcher(jdbcUrlStr);

        String dbName = matcher.find()
                ? matcher.group("dbName")
                : null;

        if (StringUtils.isBlank(dbName)) {
            throw new IllegalStateException(
                    "Failed to extract a DB name from URL %s".formatted(jdbcUrlStr)
            );
        }

        return dbName;
    }
}
