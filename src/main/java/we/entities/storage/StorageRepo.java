package we.entities.storage;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import we.orm.singleStringId.AbstractSingleStringIdRepo;

@Component
@Transactional
public class StorageRepo extends AbstractSingleStringIdRepo<Storage> {

    public StorageRepo() {
        super(Storage.class);
    }

    public Storage create(String name) {
        Storage item = new Storage();
        item.setName(name);
        super.create(item);
        return item;
    }
}
