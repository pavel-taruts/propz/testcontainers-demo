package we.entities.storage.item.transaction;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import we.orm.multiId.AbstractMultiIdRepo;

@Component
@Transactional
public class StorageItemTransactionRepo extends AbstractMultiIdRepo<StorageItemTransaction> {

    public StorageItemTransactionRepo() {
        super(StorageItemTransaction.class);
    }
}
