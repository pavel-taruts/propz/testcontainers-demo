package we.entities.storage.item.movement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import we.entities.storage.item.balance.StorageItemBalance;
import we.entities.storage.item.balance.StorageItemBalanceRepo;
import we.entities.storage.item.transaction.StorageItemTransaction;
import we.entities.storage.item.transaction.StorageItemTransactionRepo;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Service
public class StorageMovementService {

    @Autowired
    private StorageMovementRepo storageMovementRepo;

    @Autowired
    private StorageItemBalanceRepo storageItemBalanceRepo;

    @Autowired
    private StorageItemTransactionRepo storageItemTransactionRepo;

    @Transactional
    public void create(StorageMovement storageMovement) {
        ZonedDateTime now = Instant.now().atZone(ZoneOffset.UTC);
        storageMovement.setDate(now);

        storageMovement = storageMovementRepo.create(storageMovement);

        String storageFromId = storageMovement.getStorageFromId();
        String storageToId = storageMovement.getStorageToId();

        Assert.isTrue(
                storageFromId != null || storageToId != null,
                "At least one out of storageFromId and storageToId must be specified"
        );

        if (storageFromId != null) {
            createTransaction(
                    storageMovement,
                    1,
                    storageFromId,
                    storageMovement.getAmount().negate()
            );
        }

        if (storageToId != null) {
            createTransaction(
                    storageMovement,
                    2,
                    storageToId,
                    storageMovement.getAmount()
            );
        }
    }

    private void createTransaction(
            StorageMovement storageMovement,
            int transactionNumber,
            String storageId,
            BigDecimal amount
    ) {
        StorageItemTransaction transaction = justCreateTransaction(storageMovement, transactionNumber, storageId, amount);
        applyTransactionToBalance(transaction);
    }

    private StorageItemTransaction justCreateTransaction(
            StorageMovement storageMovement,
            int transactionNumber,
            String storageId,
            BigDecimal amount
    ) {
        StorageItemTransaction transaction = new StorageItemTransaction();
        transaction.setDate(storageMovement.getDate());
        transaction.setRegistrarId(storageMovement.getId());
        transaction.setItemId(storageMovement.getItemId());
        transaction.setTransactionNumber(transactionNumber);
        transaction.setStorageId(storageId);
        transaction.setAmount(amount);
        storageItemTransactionRepo.create(transaction);

        return transaction;
    }

    private void applyTransactionToBalance(StorageItemTransaction transaction) {

        // Get balance
        StorageItemBalance storageItemBalance = storageItemBalanceRepo.get(
                transaction.getStorageId(),
                transaction.getItemId()
        );

        boolean newBalance = storageItemBalance == null;

        if (newBalance) {
            storageItemBalance = new StorageItemBalance();
            storageItemBalance.setItemId(transaction.getItemId());
            storageItemBalance.setStorageId(transaction.getStorageId());
            storageItemBalance.setAmount(BigDecimal.ZERO);
        }

        // Add to the balance
        BigDecimal balance = storageItemBalance.getAmount();
        balance = balance.add(transaction.getAmount());
        storageItemBalance.setAmount(balance);

        // Save
        if (newBalance) {
            storageItemBalanceRepo.create(storageItemBalance);
        } else {
            storageItemBalanceRepo.update(storageItemBalance);
        }
    }
}
