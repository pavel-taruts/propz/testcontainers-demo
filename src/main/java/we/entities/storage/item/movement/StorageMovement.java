package we.entities.storage.item.movement;

import we.orm.singleStringId.HasStringId;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class StorageMovement implements HasStringId {

    private String id;
    private ZonedDateTime date;
    private String itemId;
    private String storageFromId;
    private String storageToId;
    private BigDecimal amount;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStorageFromId() {
        return storageFromId;
    }

    public void setStorageFromId(String storageFromId) {
        this.storageFromId = storageFromId;
    }

    public String getStorageToId() {
        return storageToId;
    }

    public void setStorageToId(String storageToId) {
        this.storageToId = storageToId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
