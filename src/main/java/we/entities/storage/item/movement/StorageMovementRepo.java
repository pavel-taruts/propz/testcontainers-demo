package we.entities.storage.item.movement;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import we.orm.singleStringId.AbstractSingleStringIdRepo;

@Component
@Transactional
public class StorageMovementRepo extends AbstractSingleStringIdRepo<StorageMovement> {

    public StorageMovementRepo() {
        super(StorageMovement.class);
    }
}
