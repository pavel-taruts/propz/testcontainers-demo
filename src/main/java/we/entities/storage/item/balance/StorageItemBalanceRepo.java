package we.entities.storage.item.balance;

import org.springframework.stereotype.Component;
import we.orm.multiId.AbstractMultiIdRepo;

import java.math.BigDecimal;

@Component
public class StorageItemBalanceRepo extends AbstractMultiIdRepo<StorageItemBalance> {

    public StorageItemBalanceRepo() {
        super(StorageItemBalance.class);
    }

    public StorageItemBalance get(String storageId, String itemId) {
        StorageItemBalance ids = new StorageItemBalance();
        ids.setStorageId(storageId);
        ids.setItemId(itemId);
        return get(ids);
    }

    public BigDecimal getAmount(String storageId, String itemId) {
        StorageItemBalance storageItemBalance = get(storageId, itemId);
        return storageItemBalance == null ? BigDecimal.ZERO : storageItemBalance.getAmount();
    }
}
