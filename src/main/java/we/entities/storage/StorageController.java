package we.entities.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/storage")
public class StorageController {

    @Autowired
    private StorageRepo storageRepo;

    @PostMapping
    public Storage create(@RequestBody Storage storage) {
        storageRepo.create(storage);
        return storage;
    }
}
