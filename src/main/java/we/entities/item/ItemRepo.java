package we.entities.item;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import we.orm.singleStringId.AbstractSingleStringIdRepo;

@Component
@Transactional
public class ItemRepo extends AbstractSingleStringIdRepo<Item> {

    public ItemRepo() {
        super(Item.class);
    }

    public Item create(String name) {
        Item item = new Item();
        item.setName(name);
        super.create(item);
        return item;
    }
}
