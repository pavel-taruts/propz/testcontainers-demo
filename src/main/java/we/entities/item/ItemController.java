package we.entities.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/item")
public class ItemController {

    @Autowired
    private ItemRepo itemRepo;

    @PostMapping
    public Item create(@RequestBody Item item) {
        itemRepo.create(item);
        return item;
    }
}
