package we.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.JdbcTransactionManager;
import we.utils.DatabaseUriUtils;
import we.utils.EnvironmentUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
public class OurConfiguration {

    @Bean
    DataSource dataSource(EnvironmentUtils environmentUtils) throws SQLException, LiquibaseException {

        Map<String, String> hikariPropertiesMap = environmentUtils.getPropertiesAndRemovePrefix("hikari.");

        Properties hikariProperties = new Properties();
        hikariProperties.putAll(hikariPropertiesMap);

        HikariConfig hikariConfig = new HikariConfig(hikariProperties);

        String jdbcUrlStr = (String) hikariProperties.get("jdbcUrl");
        String databaseName = DatabaseUriUtils.getDatabaseName(jdbcUrlStr);

        hikariConfig.addDataSourceProperty("databaseName", databaseName);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        runLiquibase(dataSource);

        return dataSource;
    }

    @Bean
    JdbcTransactionManager transactionManager(DataSource dataSource) {
        return new JdbcTransactionManager(dataSource);
    }

    @Bean
    NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    ObjectMapper objectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .indentOutput(true)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd"))
                .modulesToInstall(
                        new Jdk8Module(),
                        new JavaTimeModule(),
                        new ParameterNamesModule()
                ).build();
    }

    private void runLiquibase(HikariDataSource dataSource) throws SQLException, LiquibaseException {
        try (Connection connection = dataSource.getConnection()) {
            Liquibase liquibase = new Liquibase(
                    "liquibase/changelog.sql",
                    new ClassLoaderResourceAccessor(),
                    new JdbcConnection(connection)
            );
            liquibase.update((String) null);
        }
    }
}
