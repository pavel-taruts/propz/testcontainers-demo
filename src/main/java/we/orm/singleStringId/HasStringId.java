package we.orm.singleStringId;

public interface HasStringId {

    String getId();

    void setId(String id);
}
