package we.orm.singleStringId;

import org.apache.commons.lang3.StringUtils;
import we.orm.AbstractRepo;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class AbstractSingleStringIdRepo<T extends HasStringId> extends AbstractRepo<T> {

    private final List<Field> getIdFields;

    public AbstractSingleStringIdRepo(Class<T> clazz) {
        super(clazz);

        try {
            this.getIdFields = Collections.singletonList(clazz.getDeclaredField("id"));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("id field not found in class " + clazz.getSimpleName(), e);
        }
    }

    @Override
    protected List<Field> getIdFields() {
        return getIdFields;
    }

    @Override
    public T create(T item) {
        if (StringUtils.isBlank(item.getId())) {
            item.setId(UUID.randomUUID().toString());
        }
        return super.create(item);
    }

    public T get(String id) {
        try {
            T ids = clazz.getConstructor().newInstance();
            ids.setId(id);
            return super.get(ids);
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException(e);
        }
    }
}
