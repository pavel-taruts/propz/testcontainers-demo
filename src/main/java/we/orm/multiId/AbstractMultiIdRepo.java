package we.orm.multiId;

import we.orm.AbstractRepo;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class AbstractMultiIdRepo<T> extends AbstractRepo<T> {

    public AbstractMultiIdRepo(Class<T> clazz) {
        super(clazz);
    }

    @Override
    protected List<Field> getIdFields() {
        return Arrays
                .stream(clazz.getDeclaredFields())
                .filter(declaredField -> declaredField.getAnnotation(ID.class) != null)
                .toList();
    }
}
