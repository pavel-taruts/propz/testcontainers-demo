package we.orm;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.List;

@Transactional
public abstract class AbstractRepo<T> {
    protected final Class<T> clazz;

    private final String tableName;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AbstractRepo(Class<T> clazz) {
        this.clazz = clazz;
        this.tableName = underscoreName(clazz.getSimpleName());
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public T create(T item) {
        BeanPropertySqlParameterSource paramSource = new BeanPropertySqlParameterSource(item);
        StringBuilder sb = new StringBuilder("insert into ").append(tableName);
        appendSet(sb);
        String sql = sb.toString();
        namedParameterJdbcTemplate.update(sql, paramSource);
        return item;
    }

    public void update(T item) {
        BeanPropertySqlParameterSource paramSource = new BeanPropertySqlParameterSource(item);
        StringBuilder sb = new StringBuilder("update ").append(tableName);
        appendSet(sb);

        appendWhereIds(sb);

        String sql = sb.toString();
        namedParameterJdbcTemplate.update(sql, paramSource);
    }

    public T get(T ids) {

        StringBuilder sb = new StringBuilder("select * from ").append(tableName);

        appendWhereIds(sb);

        String sql = sb.toString();

        List<T> list = namedParameterJdbcTemplate.query(
                sql,
                new BeanPropertySqlParameterSource(ids),
                new BeanPropertyRowMapper<>(clazz)
        );

        return list.isEmpty() ? null : list.get(0);
    }

    public List<T> list() {
        return namedParameterJdbcTemplate.query(
                "select * from " + tableName,
                new EmptySqlParameterSource(),
                new BeanPropertyRowMapper<>(clazz)
        );
    }

    protected String underscoreName(String name) {

        if (StringUtils.isBlank(name)) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (Character.isUpperCase(c)) {
                if (i > 0) {
                    sb.append('_');
                }
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    protected abstract List<Field> getIdFields();

    private void appendWhereIds(StringBuilder sb) {
        List<String> idFields = getIdFieldNames();

        sb.append(" where true");

        for (String idFieldName : idFields) {
            String idColumnName = underscoreName(idFieldName);
            sb.append(" and ").append(idColumnName).append(" = :").append(idFieldName);
        }
    }

    private void appendSet(StringBuilder sb) {
        sb.append(" set ");

        Field[] declaredFields = clazz.getDeclaredFields();

        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            String parameterName = field.getName();
            String columnName = underscoreName(parameterName);
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(columnName).append(" = :").append(parameterName);
        }
    }

    private List<String> getIdFieldNames() {
        List<Field> fields = getIdFields();
        return fields
                .stream()
                .map(Field::getName)
                .toList();
    }
}
