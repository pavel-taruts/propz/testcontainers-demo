-- liquibase formatted sql

-- changeset pavel.taruts:create-table-storage

create table storage
(
    id   varchar(36)  not null primary key,
    name varchar(255) not null
);

-- changeset pavel.taruts:create-table-item

create table item
(
    id   varchar(36)  not null primary key,
    name varchar(255) not null
);

-- changeset pavel.taruts:create-table-storage_movement

create table storage_movement
(
    id              varchar(36)    not null primary key,
    date            timestamp      not null,

    item_id         varchar(36)    not null,

    storage_from_id varchar(36)    null,
    storage_to_id   varchar(36)    null,

    amount          decimal(20, 4) not null,

    foreign key fk_storage_movement__item (item_id) references item (id),
    foreign key fk_storage_movement__storage_from (storage_from_id) references storage (id),
    foreign key fk_storage_movement__storage_to (storage_to_id) references storage (id)
);

-- changeset pavel.taruts:create-table-storage_item_transaction

create table storage_item_transaction
(
    date               timestamp      not null,

    registrar_id       varchar(36)    not null,
    transaction_number int            not null,

    item_id            varchar(36)    not null,
    storage_id         varchar(36)    not null,

    amount             decimal(20, 4) not null,

    foreign key fk_storage_item_transaction__registrar (registrar_id) references storage_movement (id),
    foreign key fk_storage_item_transaction__item (item_id) references item (id),
    foreign key fk_storage_item_transaction__storage (storage_id) references storage (id),

    primary key pk_storage_item_transaction (registrar_id, transaction_number)
);

-- changeset pavel.taruts:create-table-storage_item_balance

create table storage_item_balance
(
    item_id    varchar(36)    not null,
    storage_id varchar(36)    not null,
    amount     decimal(20, 4) not null,

    foreign key fk_storage_item_balance__item (item_id) references item (id),
    foreign key fk_storage_item_balance__storage (storage_id) references storage (id),
    primary key pk_storage_item_balance (item_id, storage_id)
);
